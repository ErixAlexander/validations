class Validation {
  constructor() { }

  private _tryParseNumberToString(variable: number | string): string {
    return (typeof variable === 'number')
      ? variable.toString()
      : variable;
  }

  public getClassOf(obj: any): string {
    return Object.prototype.toString.call(obj).slice(8, -1);
  }

  public isValidNIF(nif: string): boolean {
    const regexNIF = /^[XYZ]?\d{5,8}[A-Z]$/,
      formatedNIF = nif.toUpperCase().replace(/ /g, '');
    if (!regexNIF.test(formatedNIF)) {
      return false;
    }
    const strNifWords = 'TRWAGMYFPDXBNJZSQVHLCKET',
      word = formatedNIF.substr(formatedNIF.length - 1, 1);
    let number = formatedNIF.substr(0, formatedNIF.length - 1);
    number = number.replace('X', '0');
    number = number.replace('Y', '1');
    number = number.replace('Z', '2');
    if (word !== strNifWords.charAt(parseInt(number) % 23)) {
      return false;
    }
    return true;
  }

  public isValidPhoneNumber(phoneNumber: string): boolean {
    if (typeof phoneNumber !== 'string' || phoneNumber.length !== 9) {
      return false;
    }
    return this.hasOnlyNumbers(parseInt(phoneNumber));
  }

  public isValidSSNumber(ssNumber: number): boolean {
    if (ssNumber.toString().length !== 12) {
      return false;
    }
    return this.hasOnlyNumbers(ssNumber);
  }

  public isValidEmail(email: string): boolean {
    const re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
  }

  public isValidCP(cp: string): boolean {
    if (cp.length !== 5) {
      return false;
    }
    return this.hasOnlyNumbers(parseInt(cp));
  }

  public isValidLocation(strLocation: string): boolean {
    return strLocation.length <= 40 && !this.hasNumber(strLocation) ? true : false;
  }

  public hasOnlyNumbers(value: number): boolean {
    return (isNaN(value) || isNaN(parseFloat(value.toString()))) ? false : true;
  }

  public hasNumber(string: string): boolean {
    return /\d/.test(string);
  }

  public hasSpecialChars(string: string): boolean {
    return /[`!@#ç$%^&*()_+\-=\[\]{};':"\\|,.<>\/?~]/.test(string)
  }

  public hasCertainLength(variable: number | string, length: number): boolean {
    variable = this._tryParseNumberToString(variable);
    return variable.length === length ? true : false;
  }

  public hasCertainInterval(variable: string | number, min: number, max: number) {
    variable = this._tryParseNumberToString(variable);
    return variable.length >= min && variable.length <= max ? true : false;
  }
}